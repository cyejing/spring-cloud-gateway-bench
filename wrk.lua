
function response(status, headers, body)
    if (body~="hello\n" or status>=400) then
        io.write("reponse error", status, headers, body)
    end
end

done = function(summary, latency, requests)

    io.write("--------------------------\n")
    local durations=summary.duration / 1000000
    local errors=summary.errors.status
    local total=summary.requests
    local valid=total-errors


    io.write("Durations:       "..string.format("%.2f",durations).."s".."\n")
    io.write("Requests:        "..summary.requests.."\n")
    io.write("Avg RT:          "..string.format("%.2f",latency.mean / 1000).."ms".."\n")
    io.write("Max RT:          "..(latency.max / 1000).."ms".."\n")
    io.write("Min RT:          "..(latency.min / 1000).."ms".."\n")
    io.write("Error requests:  "..errors.."\n")
    io.write("Valid requests:  "..valid.."\n")
    io.write("MAX-QPS/THREAD:  "..string.format("%.2f",requests.max).."\n")
    io.write("AVG-QPS:         "..string.format("%.2f",valid / durations).."\n")
    io.write("--------------------------\n")

end
