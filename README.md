Gateway Benchmark
=======

TL;DR

Proxy | Avg Latency | Avg Req/Sec/Thread
-- | -- | --
dam | 2.04ms | 107.869k
gateway | 4.68ms | 43.827k
linkered | 5.23ms | 41.988k
zuul | 11.08ms | 22.757k
none | 3.25ms | 161.243k

## Terminal 1 (simple webserver)

```bash
cd static
./webserver # or ./webserver.darwin-amd64 on a mac
```

## Terminal 2 (zuul)
```bash
cd zuul
./mvnw clean package
java -jar target/zuul-0.0.1-SNAPSHOT.jar 
```

## Terminal 3 (gateway)
```bash
cd gateway
./mvnw clean package
java -jar target/gateway-0.0.1-SNAPSHOT.jar 
```

## Terminal 4 (linkerd)
```bash
cd linkerd
java -jar linkerd-1.3.4.jar linkerd.yaml
```

## Terminal 5 (dam)
```bash
cd dam
./mvnw clean package
java -jar target/dam-1.0-SNAPSHOT.jar
```

## Terminal N (wrk)

### install `wrk`
Ubuntu: `sudo apt install wrk`

Mac: `brew install wrk`

NOTE: run each one multiple times to warm up jvm

### Gateway bench (8082)
```bash
$ wrk -t 10 -c 200 -d 30s http://localhost:8082/hello.txt

```

### Dam bench (8048)
```bash
$ wrk -t 10 -c 200 -d 30s http://localhost:8048/hello.txt

```

### zuul bench (8081)
```bash
$ wrk -t 10 -c 200 -d 30s http://localhost:8081/hello.txt

```

### linkerd bench (4140)
```bash
$ wrk -H "Host: web" -t 10 -c 200 -d 30s http://localhost:4140/hello.txt

```

### no proxy bench (8000)
```bash
$ wrk -t 10 -c 200 -d 30s http://localhost:8000/hello.txt

```
